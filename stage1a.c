#include <stdio.h> 
#include <string.h> 
#include <stdint.h> 
#include <stdlib.h> 
#include <math.h>

// display the prompt for user
void displayPrompt();
// will exit the program anytime it is called
void exitProgram();

// main function
int main(){
 while(1){  // continously call the the user prompt for a constant stream of input from the user 	
   displayPrompt();
 }
return(0);
}

// Display a prompt for the user and be able to get input from the user
// Stop's when user types "exit" or when Ctrl-D is pressed
void displayPrompt(){

 char input[513]; //input thats 512 characters long
 const char delimiter[9] = "; |\n\t&<>"; // qutations for space
 char *token; // a token that will store the input and used to check input
 int found = 1; 

 printf("Please Prompt>");//print User prompt to consle 

 if((fgets(input,513,stdin)) == NULL){ // Ctrl-D makes fgets return a NULL so this checks for if Ctrl-D has been pressed
    printf("\n");
    exitProgram();
   
 } else {
for(int i = 0; i<512;i++){
 if (input[i] == '\0'){
  found = 0;
  }
 }
if(found == 1){
 printf("Error char limit reached with the following characters:");
 fgets(input,513,stdin);
}
if (!strcmp(input,"exit\n")){ // checks to see if the input is "exit" and if so exit the program
       exitProgram();

	} 
    token = strtok(input,delimiter);// token is set to the input including the space's
 if (input[0] == '\n' && input[1] == '\0'){ // checks if the input is emtpy and if so display that no input was detected
       printf("No input detected\n");
    }else{

 while( token != NULL ) { // if the token is not null then print out the token then set it to null after it has printed
     		printf( " '%s'\n", token );
     		token = strtok(NULL, delimiter);
    }
   }
  }
 }

void exitProgram(){

printf("Exiting program....\n");
exit(0);

}
